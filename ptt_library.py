import requests

user_agent = "com.pttxd"
international_url = "https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/yurtDisiKargoSorgulaMSAEHPREMHMRGBAGDOGMAMA"
national_url = "https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/gonderisorgu2MSAEHPREMHMRGBAGDOGMAMA"
general_url = "https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/gonderisorguYurticiveYurtDisi"

headers = {"User-Agent": user_agent}


def do_international_query(barcode):
    r = requests.post(international_url, data={"barkod": barcode, "tokenIdBildirim": ""}, headers=headers)
    return r.json()


def do_national_query(barcode):
    r = requests.post(national_url, data={"barkod": barcode, "tokenIdBildirim": ""}, headers=headers)
    return r.json()


def do_query(barcode):
    r = requests.post(general_url, data={"kaynak": "ANDROID", "barkod": barcode, "tokenIdBildirim": ""}, headers=headers)
    return r.json()

