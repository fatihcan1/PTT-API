# Unofficial PTT Tracking API Docs

**Original/up-to-date version of this file can be found on https://gitlab.com/a/ptt-api**

Please keep in mind that PTT isn't the best at keeping servers up and these APIs do occasionally go down. Be prepared for it.

_PTT also isn't the best for keeping the mime type properly, so do check the `Content-Type` header before parsing!_

### POST `https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/gonderisorguYurticiveYurtDisi`

- Requires `User-Agent: com.pttxd`

Returns detailed JSON information about the given national or international shipment. Compared to the specific ones, this one actually includes customs information too.

#### Input

Input is form data. Replace `AB123456789CD` with your tracking number.
`tokenIdBildirim` is empty.

```
barkod: AB123456789CD
kaynak: ANDROID
tokenIdBildirim: ""
```

#### Output

**When a proper tracking number is given and there's information:**

HTTP 200

```js
{
    "ALICI": "N**** O**", // Intended Recipient
    "BARNO": "AB123456789CD", // Barcode
    "GONDEREN": "N**** O**", // Sender
    "GONUCR": "",
    "GR": "1234", // Weight in grams, 0 for envelopes
    "IMERK": "KONYA ULUSLARARASI PİM",
    "TESALAN": "", // Actual Recipient
    "VMERK": "",
    "dongu": [ // this was trimmed by me to keep things short
        {
            "event": "The item was forwarded to the destination country (Otb)",
            "islemYeri": "Lausanne (Yurt Disi)",
            "no": "31/12/23     13:37",
            "tarih": "2023-12-31 13:37:12",
            "yapilanIslem": "Gonderi yurt disina sevk edildi (Giden)",
            "__hashCodeCalc": false
        },
        {
            "event": "Deliver item (Inb)",
            "islemYeri": "TÜRKIYE",
            "no": "31/12/23     13:37",
            "ofis": "KONYA PDM",
            "tarih": "2023-12-31 13:37:34",
            "yapilanIslem": "Gonderi teslim edildi (Gelen)",
            "__hashCodeCalc": false
        }
    ],
    "sonucAciklama": "Basarili",
    "sonucKodu": 0,
    "GUMRUKTUR": "4", // Type of shipment for Customs (known: KOLİ for parcels, 4 for small boxes which are still taxed, , null for envelopes and other non-taxed shipments)
    "GUMRUKSUNMAUCR": "5.2", // Cost of presenting to Customs
    "GUMRUKDAMGAUCR": "0.8", // Stamp cost from Customs
    "GUMRUKVERGIUCR": "5.11", // Tax cost from Customs
    "GUMRUKTOPLAMUCR": "11.010000000000002", // Total cost from Customs. Yep. Floating point numbers are fun.
    "GUMRUKIADEUCR": "0.0", // Customs refund amount
    "__hashCodeCalc": false
}
```

**When given tracking number is correct but there's no information:**

HTTP 200

```js
{
    "dongu": [],
    "sonucAciklama": "Barkod Numarasna Ait İslem Bilgisi Bulunamadı.",
    "teslim_alan": "",
    "teslim_durumu": "Bu Gönderiye Ait Teslim Bilgisi Bulunamadi ( There is no delivery information for the item)",
    "__hashCodeCalc": false
}
```

**When given tracking number is incorrect:**

HTTP 200 (why)

```js
{
    "dongu": [],
    "sonucAciklama": "Barkod Numarasna Ait İslem Bilgisi Bulunamadı.",
    "teslim_alan": "",
    "teslim_durumu": "Bu Gönderiye Ait Teslim Bilgisi Bulunamadi ( There is no delivery information for the item)",
    "__hashCodeCalc": false
}
```

**When tracking number is missing:**

HTTP 500

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Application error</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <body>
        <h1>Oops, an error occured</h1>
        <p>
    This exception has been logged with id 
            <strong>123abcd45</strong>.
        </p>
    </body>
</html>
```


### POST `https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/yurtDisiKargoSorgulaMSAEHPREMHMRGBAGDOGMAMA`

- Requires `User-Agent: com.pttxd`

Returns JSON information about the given international shipment.

#### Input

Input is form data. Replace `AB123456789CD` with your tracking number.
`tokenIdBildirim` is empty.

```
barkod: AB123456789CD
tokenIdBildirim: ""
```

#### Output

**When a proper tracking number is given and there's information:**

HTTP 200

```js
{
    "dongu": [ // this was trimmed by me to keep things short
        {
            "event": "The item was forwarded to the destination country (Otb)",
            "islemYeri": "Lausanne (Yurt Disi)",
            "no": "31/12/23     13:37",
            "tarih": "2023-12-31 13:37:12",
            "yapilanIslem": "Gonderi yurt disina sevk edildi (Giden)",
            "__hashCodeCalc": false
        },
        {
            "event": "Deliver item (Inb)",
            "islemYeri": "TÜRKIYE",
            "no": "31/12/23     13:37",
            "ofis": "KONYA PDM",
            "tarih": "2023-12-31 13:37:34",
            "yapilanIslem": "Gonderi teslim edildi (Gelen)",
            "__hashCodeCalc": false
        }
    ],
    "sonucAciklama": "Barkod Numarasna Ait İslem Listesi Bulundu",
    "teslim_alan": " [REDACTED]", // recipient
    "teslim_durumu": "Bu Gönderiye Ait Teslim Bilgisi Bulunamadi ( There is no delivery information for the item)",
    "__hashCodeCalc": false
}
```

**When given tracking number is correct but there's no information:**

HTTP 200

```js
{
    "dongu": [],
    "sonucAciklama": "Barkod Numarasna Ait İslem Bilgisi Bulunamadı.",
    "teslim_alan": "",
    "teslim_durumu": "Bu Gönderiye Ait Teslim Bilgisi Bulunamadi ( There is no delivery information for the item)",
    "__hashCodeCalc": false
}
```

**When given tracking number is incorrect:**

HTTP 200 (why)

```js
{
    "dongu": [],
    "sonucAciklama": "Barkod Numarasna Ait İslem Bilgisi Bulunamadı.",
    "teslim_alan": "",
    "teslim_durumu": "Bu Gönderiye Ait Teslim Bilgisi Bulunamadi ( There is no delivery information for the item)",
    "__hashCodeCalc": false
}
```

**When tracking number is missing:**

HTTP 500

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Application error</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <body>
        <h1>Oops, an error occured</h1>
        <p>
    This exception has been logged with id 
            <strong>123abcd45</strong>.
        </p>
    </body>
</html>
```

### POST `https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/gonderisorgu2MSAEHPREMHMRGBAGDOGMAMA`

- Requires `User-Agent: com.pttxd`

Returns JSON information about the given international or domestic shipment.

I recommend only using this for domestic ones.

For international shipments, only shows actions inside the country.

#### Input

Input is form data. Replace `AB123456789CD` with your tracking number.
`tokenIdBildirim` is empty.

```
barkod: AB123456789CD
tokenIdBildirim: ""
```

#### Output

**When a proper tracking number is given and there's information:**

```js
{
    "ALICI": "N**** O**", // intended recipient
    "BARNO": "AB123456789CD",
    "DEGKONUCR": " TL",
    "EKHIZ": " ",
    "GONDEREN": "O*** O*** F***",
    "GONUCR": "-",
    "GR": "1Kg/0.00D.",
    "IMERK": " / ",
    "ITARIH": "20231231",
    "ODSARUCR": " TL",
    "TESALAN": "N**** O**", // actual recipient
    "VMERK": "REDACTED", // no clue what it refers to, it's PII, redacted
    "dongu": [ // this was trimmed by me to keep things short
        {
            "IMERK": "İST.ULUSLARARASI PİM/İST.ULUSLARARASI PİM",
            "ISAAT": "13:37:12",
            "ISLEM": "Yurtdışı Kabul Geliş Kaydı Yapıldı",
            "ITARIH": "31/12/2023",
            "siraNo": 1,
            "__hashCodeCalc": false
        },
        {
            "IMERK": "KONYA",
            "ISAAT": "13:37:24",
            "ISLEM": "Teslim Edildi",
            "ITARIH": "31/12/2023",
            "siraNo": 2,
            "__hashCodeCalc": false
        }
    ],
    "sonucAciklama": "islem basarili",
    "sonucKodu": 10,
    "__hashCodeCalc": false
}
```

**When given tracking number is correct but there's no information:**

HTTP 200

```js
{
    "GONUCR": "-",
    "dongu": [],
    "__hashCodeCalc": false
}
```

**When given tracking number is incorrect:**

HTTP 200 (why)

```js
{
    "GONUCR": "-",
    "dongu": [],
    "sonucAciklama": "BARKOD HATALI",
    "sonucKodu": 1,
    "__hashCodeCalc": false
}
```

**When tracking number is missing:**

HTTP 500

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Application error</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <body>
        <h1>Oops, an error occured</h1>
        <p>
    This exception has been logged with id 
            <strong>123abcd45</strong>.
        </p>
    </body>
</html>
```
